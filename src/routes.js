import React from "react";
import { IndexRoute, Route } from "react-router";
import {
  isLoaded as isAuthLoaded,
  load as loadAuth,
  loadPrevUrl,
} from "redux/modules/auth";
import {
  App,
  Chat,
  Widgets,
  About,
  Login,
  LoginSuccess,
  Survey,
  NotFound,
  Pagination,
} from "containers";
import Home from "./containers/Home/Home";
import Collection from "./containers/Collection/Collection";
import OrderLifeCycle from "./containers/OrderLifeCycle/OrderLifeCycle";

export default (store) => {
  var lastLocation = "";

  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route path="/" component={App}>
      {/* Home (main) route */}
      <IndexRoute component={Login} />

      {/* Routes requiring login */}
      <Route>
        <Route path="chat" component={Chat} />
        <Route path="loginSuccess" component={LoginSuccess} />
        <Route path="cart" component={Home} />
        <Route path="orders" component={OrderLifeCycle} />
        <Route path="collections/:id" component={Collection} />
      </Route>

      {/* Routes */}
      <Route path="about" component={About} />
      <Route path="login" component={Login} />
      <Route path="pagination" component={Pagination} />
      <Route path="survey" component={Survey} />
      <Route path="widgets" component={Widgets} />

      {/* Catch all route */}
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
