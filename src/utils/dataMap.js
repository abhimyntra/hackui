
export const dataMap ={
    "0000-0000-0000-0000": {
      "id": 11461412,
      "name": "Artificial Nail Art Kit",
      "brandName": "Rozia",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/a6b2387a-d137-4fe1-a112-4924ed69dda01580854974644-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/5/a6b2387a-d137-4fe1-a112-4924ed69dda01580854974644-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/a6b2387a-d137-4fe1-a112-4924ed69dda01580854974644-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/5/a6b2387a-d137-4fe1-a112-4924ed69dda01580854974644-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/39772b41-ae39-4343-9b51-6ed8fdf7aa5a1580854974846-5.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/5/39772b41-ae39-4343-9b51-6ed8fdf7aa5a1580854974846-5.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/40d0967d-c9ed-414e-9b71-d62776fd7cf71580854974729-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/5/40d0967d-c9ed-414e-9b71-d62776fd7cf71580854974729-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/1833d0cb-3300-4d37-a5f2-a278f0d19e801580854974688-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/5/1833d0cb-3300-4d37-a5f2-a278f0d19e801580854974688-2.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/8214ba73-5b59-49ec-87e4-f30a2df0b0a51580854974771-4.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/5/8214ba73-5b59-49ec-87e4-f30a2df0b0a51580854974771-4.jpg"
        }
      ],
      "price": 284,
      "mrp": 299,
      "inventoryInfo": [
        {
          "skuId": 39824154,
          "label": "Onesize",
          "inventory": 26,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 15,
      "discountDisplayLabel": "(5% OFF)",
      "searchImage": "http://assets.myntassets.com/assets/images/productimage/2020/2/5/a6b2387a-d137-4fe1-a112-4924ed69dda01580854974644-1.jpg"
    },
    "1111-1111-1111-1111": {
      "id": 11705582,
      "name": "Wild Muse Eye Powder ES15",
      "brandName": "BIOTIQUE NATURAL MAKEUP",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/11705582/2020/5/26/104039ba-1220-4fb8-90cb-024350eb05e91590489744453-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/11705582/2020/5/26/104039ba-1220-4fb8-90cb-024350eb05e91590489744453-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/11705582/2020/5/26/52aede55-4aae-41f2-af5c-7c795c81d6411590489744317-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/11705582/2020/5/26/52aede55-4aae-41f2-af5c-7c795c81d6411590489744317-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/11705582/2020/5/26/104039ba-1220-4fb8-90cb-024350eb05e91590489744453-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/11705582/2020/5/26/104039ba-1220-4fb8-90cb-024350eb05e91590489744453-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/11705582/2020/5/26/f7c3ab03-23c3-4a82-a417-d48677e274ae1590489744393-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/11705582/2020/5/26/f7c3ab03-23c3-4a82-a417-d48677e274ae1590489744393-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-2.jpg"
        }
      ],
      "price": 440,
      "mrp": 440,
      "inventoryInfo": [
        {
          "skuId": 40704516,
          "label": "Onesize",
          "inventory": 15,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 0,
      "discountDisplayLabel": "",
      "searchImage": "http://assets.myntassets.com/assets/images/11705582/2020/5/26/104039ba-1220-4fb8-90cb-024350eb05e91590489744453-BIOTIQUE-NATURAL-MAKEUP-Diva-Shimmer-Wild-Muse-Sparkling-Eye-1.jpg"
    },
    "2222-2222-2222-2222": {
      "id": 10564426,
      "name": "Set of 2 Alligator Hair Clips",
      "brandName": "Priyaasi",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/10564426/2020/1/6/d2b6bb94-e120-4c01-8582-d96b9fbc692e1578308846925-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10564426/2020/1/6/d2b6bb94-e120-4c01-8582-d96b9fbc692e1578308846925-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10564426/2020/1/6/d2b6bb94-e120-4c01-8582-d96b9fbc692e1578308846925-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10564426/2020/1/6/d2b6bb94-e120-4c01-8582-d96b9fbc692e1578308846925-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10564426/2020/1/6/07321f25-64be-4663-9d0b-ae59915569791578308846854-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10564426/2020/1/6/07321f25-64be-4663-9d0b-ae59915569791578308846854-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10564426/2020/1/6/f8a9500e-d519-4312-b8d0-666aa2ebde3d1578308846807-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-4.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10564426/2020/1/6/f8a9500e-d519-4312-b8d0-666aa2ebde3d1578308846807-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-4.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10564426/2020/1/6/44c4b83c-19b7-4b7d-b11d-2c4994b482e91578308846889-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10564426/2020/1/6/44c4b83c-19b7-4b7d-b11d-2c4994b482e91578308846889-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-2.jpg"
        }
      ],
      "price": 391,
      "mrp": 1030,
      "inventoryInfo": [
        {
          "skuId": 36202970,
          "label": "Onesize",
          "inventory": 24,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 639,
      "discountDisplayLabel": "(62% OFF)",
      "searchImage": "http://assets.myntassets.com/assets/images/10564426/2020/1/6/d2b6bb94-e120-4c01-8582-d96b9fbc692e1578308846925-Priyaasi-Set-of-2-Pink--White-Rose-Shaped-Alligator-Hair-Cli-1.jpg"
    },
    "3333-3333-3333-3333": {
      "id": 10188887,
      "name": "Set of 3 Embellished Hair Accessory Set",
      "brandName": "Golden Peacock",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/10188887/2019/7/10/45ad278b-eeef-4942-a172-396f9f779ad91562740813874-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10188887/2019/7/10/45ad278b-eeef-4942-a172-396f9f779ad91562740813874-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10188887/2019/7/10/17323124-354d-4441-b507-223532b441041562740813831-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10188887/2019/7/10/17323124-354d-4441-b507-223532b441041562740813831-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10188887/2019/7/10/45ad278b-eeef-4942-a172-396f9f779ad91562740813874-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10188887/2019/7/10/45ad278b-eeef-4942-a172-396f9f779ad91562740813874-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/10188887/2019/7/10/213f6689-0f49-46c9-ad6a-dd4938404b251562740813849-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/10188887/2019/7/10/213f6689-0f49-46c9-ad6a-dd4938404b251562740813849-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-2.jpg"
        }
      ],
      "price": 427,
      "mrp": 949,
      "inventoryInfo": [
        {
          "skuId": 34946122,
          "label": "Onesize",
          "inventory": 4,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 522,
      "discountDisplayLabel": "(55% OFF)",
      "searchImage": "http://assets.myntassets.com/assets/images/10188887/2019/7/10/45ad278b-eeef-4942-a172-396f9f779ad91562740813874-Golden-Peacock-Gold-Toned--Cream-Coloured-Embellished-3-Piec-1.jpg"
    },
    "4444-4444-4444-4444": {
      "id": 9384801,
      "name": "Embellished Comb Pin",
      "brandName": "Golden Peacock",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2019/4/23/c08c8a5a-6ff0-48b2-9929-5a389f391ae61556022537327-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2019/4/23/c08c8a5a-6ff0-48b2-9929-5a389f391ae61556022537327-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2019/4/23/c08c8a5a-6ff0-48b2-9929-5a389f391ae61556022537327-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2019/4/23/c08c8a5a-6ff0-48b2-9929-5a389f391ae61556022537327-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2019/4/23/7ff5414e-0d84-4833-95c2-4ec724ec43541556022537346-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2019/4/23/7ff5414e-0d84-4833-95c2-4ec724ec43541556022537346-2.jpg"
        }
      ],
      "price": 519,
      "mrp": 1299,
      "inventoryInfo": [
        {
          "skuId": 34138529,
          "label": "Onesize",
          "inventory": 9,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 780,
      "discountDisplayLabel": "(60% OFF)",
      "searchImage": "http://assets.myntassets.com/assets/images/productimage/2019/4/23/c08c8a5a-6ff0-48b2-9929-5a389f391ae61556022537327-1.jpg"
    },
    "5555-5555-5555-5555": {
      "id": 2399990,
      "name": "Printed Scarf",
      "brandName": "AURELIA",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397958-AURELIA-Women-Dupatta-4541519213397787-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397958-AURELIA-Women-Dupatta-4541519213397787-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213398008-AURELIA-Women-Dupatta-4541519213397787-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213398008-AURELIA-Women-Dupatta-4541519213397787-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397958-AURELIA-Women-Dupatta-4541519213397787-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397958-AURELIA-Women-Dupatta-4541519213397787-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397904-AURELIA-Women-Dupatta-4541519213397787-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397904-AURELIA-Women-Dupatta-4541519213397787-2.jpg"
        }
      ],
      "price": 599,
      "mrp": 599,
      "inventoryInfo": [
        {
          "skuId": 15336548,
          "label": "Onesize",
          "inventory": 3,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 0,
      "discountDisplayLabel": "",
      "searchImage": "http://assets.myntassets.com/assets/images/2399990/2018/2/21/11519213397958-AURELIA-Women-Dupatta-4541519213397787-1.jpg"
    },
    "6666-6666-6666-6666": {
      "id": 11503174,
      "name": "Colourblocked Handcrafted Bag",
      "brandName": "Diwaah",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/034ce449-5bcd-4f65-b5d3-18ae2f943d331581468875572-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/12/034ce449-5bcd-4f65-b5d3-18ae2f943d331581468875572-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/034ce449-5bcd-4f65-b5d3-18ae2f943d331581468875572-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/12/034ce449-5bcd-4f65-b5d3-18ae2f943d331581468875572-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/3aca7dd8-dd46-40bb-9b3d-ac46705998691581468875742-5.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/12/3aca7dd8-dd46-40bb-9b3d-ac46705998691581468875742-5.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/2509129f-45a8-49fc-a0cd-d344a61fcc161581468875659-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/12/2509129f-45a8-49fc-a0cd-d344a61fcc161581468875659-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/2c557364-d0e2-4d1a-b66c-6c62825bdcf71581468875615-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/12/2c557364-d0e2-4d1a-b66c-6c62825bdcf71581468875615-2.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/47e60e3c-13ed-436d-8814-726f194fc13a1581468875703-4.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2020/2/12/47e60e3c-13ed-436d-8814-726f194fc13a1581468875703-4.jpg"
        }
      ],
      "price": 349,
      "mrp": 499,
      "inventoryInfo": [
        {
          "skuId": 33551019,
          "label": "Onesize",
          "inventory": 39,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 150,
      "discountDisplayLabel": "(30% OFF)",
      "searchImage": "http://assets.myntassets.com/assets/images/productimage/2020/2/12/034ce449-5bcd-4f65-b5d3-18ae2f943d331581468875572-1.jpg"
    },
    "7777-7777-7777-7777": {
      "id": 5890285,
      "name": "Printed Scarf",
      "brandName": "shiloh",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/5890285/2018/5/15/b791f9f0-77b1-4f48-ae9a-5b7da254a2f91526365279995-shiloh-White-Printed-Scarf-6011526365279871-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/5890285/2018/5/15/b791f9f0-77b1-4f48-ae9a-5b7da254a2f91526365279995-shiloh-White-Printed-Scarf-6011526365279871-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/5890285/2018/5/15/bb2a525b-b24c-4736-842d-0f630beca42e1526365279931-shiloh-White-Printed-Scarf-6011526365279871-3.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/5890285/2018/5/15/bb2a525b-b24c-4736-842d-0f630beca42e1526365279931-shiloh-White-Printed-Scarf-6011526365279871-3.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/5890285/2018/5/15/b791f9f0-77b1-4f48-ae9a-5b7da254a2f91526365279995-shiloh-White-Printed-Scarf-6011526365279871-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/5890285/2018/5/15/b791f9f0-77b1-4f48-ae9a-5b7da254a2f91526365279995-shiloh-White-Printed-Scarf-6011526365279871-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/5890285/2018/5/15/3661b37f-963d-438f-8495-2f227d5e08a31526365279955-shiloh-White-Printed-Scarf-6011526365279871-2.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/5890285/2018/5/15/3661b37f-963d-438f-8495-2f227d5e08a31526365279955-shiloh-White-Printed-Scarf-6011526365279871-2.jpg"
        }
      ],
      "price": 299,
      "mrp": 599,
      "inventoryInfo": [
        {
          "skuId": 27866932,
          "label": "Onesize",
          "inventory": 9,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 300,
      "discountDisplayLabel": "(50% OFF)",
      "searchImage": "http://assets.myntassets.com/assets/images/5890285/2018/5/15/b791f9f0-77b1-4f48-ae9a-5b7da254a2f91526365279995-shiloh-White-Printed-Scarf-6011526365279871-1.jpg"
    },
    "8888-8888-8888-8888": {
      "id": 11021954,
      "name": "Colour Effect Fabulous Eyelashes",
      "brandName": "Cala",
      "styleImages": [
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2019/11/20/12d484fe-e042-4300-978b-89638671f05e1574197666699-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2019/11/20/12d484fe-e042-4300-978b-89638671f05e1574197666699-1.jpg"
        },
        {
          "src": "http://assets.myntassets.com/assets/images/productimage/2019/11/20/12d484fe-e042-4300-978b-89638671f05e1574197666699-1.jpg",
          "secureSrc": "https://assets.myntassets.com/assets/images/productimage/2019/11/20/12d484fe-e042-4300-978b-89638671f05e1574197666699-1.jpg"
        }
      ],
      "price": 299,
      "mrp": 299,
      "inventoryInfo": [
        {
          "skuId": 38011366,
          "label": "Onesize",
          "inventory": 2,
          "available": true
        }
      ],
      "availableSizeData": "Onesize",
      "discount": 0,
      "discountDisplayLabel": "",
      "searchImage": "http://assets.myntassets.com/assets/images/productimage/2019/11/20/12d484fe-e042-4300-978b-89638671f05e1574197666699-1.jpg"
    }
  }

