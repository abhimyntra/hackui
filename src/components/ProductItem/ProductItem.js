import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShareAltSquare, faRupeeSign, faShoppingBag } from '@fortawesome/free-solid-svg-icons';

@connect(
    state => ({user: state.auth.user, prevUrl:state.auth.prevUrl}),
    {pushState: push})
export default class ProductItem extends Component {
   constructor(props){
        super(props);
        this.state={
            itemMovedToBag:false
        };

        this.moveToBag = this.moveToBag.bind(this);
        this.redirectToCart = this.redirectToCart.bind(this);
   }

   moveToBag(){
       this.setState({itemMovedToBag:true})
   }

   redirectToCart(){
       this.props.pushState("/cart")
   }

  render() {
    const {item, buttonTitle, collectionPage} = this.props;
    const {itemMovedToBag} = this.state;
    const styles = require('./productItem.scss');
    debugger;
    return (
        <div className={styles.itemBaseItem}>
        <div className={styles.itemContainerBaseItem}>
           <div className={styles.itemContainerBaseItemLeft}>
              <a href="/Dupatta/Dupatta-Bazaar/Dupatta-Bazaar-White-Embroidered-Chiffon-Dupatta/1518329/buy?mini=true&amp;skuId=12079178&amp;sellerPartnerId=4216">
                 <div className={styles.imgContainer} >
                    <div className="LazyLoad is-visible" >
                       <picture classNameName="image-base-imgResponsive" >
                          <img src={item.styleImages[0].src} className={styles.imgResponsive} alt="image"/>
                       </picture>
                    </div>
                 </div>
              </a>
           </div>
           <div className={styles.itemContainerBaseItemRight}>
              <div className={styles.itemContainerBaseDetails}>
                 <div>
                    <div className={styles.itemContainerBaseBrand}>{item.brandName}</div>
    <a className={styles.itemContainerBaseItemLink} href="">{item.name}</a>
                 </div>
                 <div className={styles.itemComponentsBaseSellerContainer}>
                    <div className={styles.itemComponentsBaseSellerData}>Sold by: Flashtech Retail</div>
                 </div>
                 <div className="itemContainer-base-sizeAndQty">
                    <div className="itemComponents-base-size">
                       <span className="">Size: Onesize</span>
                    
                    </div>
                    <div className="itemComponents-base-quantity">
                       <span className="">Qty: 1</span>
                   
                    </div>
                 </div>
                 <div>
                    <div className="itemComponents-base-price itemComponents-base-bold itemContainer-base-amount">
                       <div>
                          449
                       </div>
                    </div>
                    <div className="itemContainer-base-discountStrikedAmount">
                       <span className="itemComponents-base-strikedAmount">
                          <span className="itemComponents-base-price itemComponents-base-strike itemContainer-base-strikedAmount">
                                
                             899
                          </span>
                       </span>
                       <span className="itemComponents-base-itemDiscount">50% OFF</span>
                    </div>
                 </div>
                 <div></div>
              </div>
           </div>
           {collectionPage ?
           <div className={styles.baseActions}>
              {!this.state.itemMovedToBag ? <div className={styles.baseBagButton} onClick={this.moveToBag}><button className="inlinebuttonV2-base-actionButton itemContainer-base-inlineButton  wishlistButton">Move to Bag</button></div>:
              <div className={styles.baseBag} ><FontAwesomeIcon className={styles.shareIcon} icon={faShoppingBag} onClick={this.redirectToCart} /></div>}
           </div> :
           <div className={styles.baseActions}>
              <div className={styles.baseRemoveButton}><button className="inlinebuttonV2-base-actionButton itemContainer-base-inlineButton removeButton">Remove</button></div>
              <div className={styles.baseMoveButton}><button className="inlinebuttonV2-base-actionButton itemContainer-base-inlineButton  wishlistButton">Move to WishList</button></div>
           </div>}
        </div>
     </div>
    );
  }
}
