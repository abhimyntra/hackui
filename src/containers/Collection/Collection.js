import React, { Component } from 'react';
import ProductItem from '../../components/ProductItem/ProductItem';
import {connect} from 'react-redux';
import {dataMap} from '../../utils/dataMap';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShareAltSquare, faRupeeSign } from '@fortawesome/free-solid-svg-icons';
import { Modal, Button } from 'react-bootstrap';
import offer from '../../../static/offerImage1.png';
import cartAmount from '../../../static/cartAmountImage.png';
import header from '../../../static/myntraHeader.png';
@connect(
  state => ({user: state.auth.user})
)
//const data = [{"id":"0000-0000-0000-0000"},{"id":"1111-1111-1111-1111"},{"id":"2222-2222-2222-2222"},{"id":"3333-3333-3333-3333"},{"id":"4444-4444-4444-4444"},{"id":"5555-5555-5555-5555"},{"id":"6666-6666-6666-6666"},{"id":"7777-7777-7777-7777"},{"id":"8888-8888-8888-8888"}]
export default class Collection extends Component {
  constructor(props){
    super(props);
    this.state = {
      data:[],
      showModal:false
    }
    this.handleClose = this.handleClose.bind(this);
    this.updateDescription = this.updateDescription.bind(this);
    this.shareCart = this.shareCart.bind(this);
  }

  componentDidMount() {
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    axios.get("hack/v0/bag",{
      crossDomain: true,
      headers:{
        userId:1111111111
      }
    })
      .then(res => {
        const data = res.data.data.itemIds;
        this.setState({ data });
      })
  }

  handleClose(){
    this.setState({showModal:false})
  }

  updateDescription(e){
      this.setState({description:e.target.value})
  }

  shareCart(){
    var shareObj = {
      owner:this.props.user,
      description:this.state.description,
      itemIds:this.state.data,
      mode:"PUBLIC"
    }
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    axios.get("hack/v0/bag/share?owner=" +this.props.user.name + "&description="+this.state.description + "&itemIds="+this.state.data)
      .then(res => {
        this.setState({shareableLink:"http://localhost:3000/collections" + res.data.data.id})
      })
  }
  render() {
    const styles = require('./Collection.scss');
    return (
      <div className={styles.home}>
        <img src={header} className={styles.myntraHeader}> </img>
        <div className={styles.homeLeft}>

          <div className={styles.pageTitle}>My Collection</div>
          <div className={styles.collectionTitle} >Sample Collection</div>           
          <div className={styles.productList}>
            {this.state.data && this.state.data.map(item=>{
                return <ProductItem item={dataMap[item]} buttonTitle={"Move to Bag"} collectionPage={true}/>
              })}
          </div>
        </div>
      </div>
    );
  }
}
