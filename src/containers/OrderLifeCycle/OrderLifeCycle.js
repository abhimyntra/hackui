import React, { Component, PropTypes } from "react";
import { Router, Route, Link, RouteHandler } from "react-router";
import { connect } from "react-redux";
import { createBrowserHistory } from "history";
import Helmet from "react-helmet";
import * as authActions from "redux/modules/auth";
import header from "../../../static/myntraHeader1.png";
import orderTop from "../../../static/orderTop.png";
import orderLeft from "../../../static/orderLeft.png";
import orderBottom from "../../../static/orderBottom.png";
import bookedLogo from "../../../static/bookedLogo.svg";
@connect((state) => ({ user: state.auth.user }), authActions)
export default class OrderLifeCycle extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
  };

  render() {
    const { user, logout } = this.props;
    const styles = require("./OrderLifeCycle.scss");
    return (
      <div className={styles.loginPage}>
        <img src={header} className={styles.myntraHeader}>
          {" "}
        </img>
        <div className={styles.leftColumn}>
          <img src={orderLeft} className={styles.orderLeft}>
            {" "}
          </img>
        </div>
        <div className={styles.rightColumn}>
          <img src={orderTop} className={styles.orderTop}>
            {" "}
          </img>
          <div className={styles.ordersList}>
            <div className={styles.myOrder}>
              <div className={styles.orderTitle}>
                <div className={styles.iconContainer}>
                  <img src={bookedLogo} className={styles.icon}></img>
                </div>
                <span className={styles.tick}></span>
                <div className={styles.itemContainer}>
                  <span className={styles.itemTitle}>Booked</span>
                  <span className={styles.itemDate}>On Fri, 29 Jan</span>
                </div>
              </div>
            </div>
          </div>
          <img src={orderBottom} className={styles.orderBottom}>
            {" "}
          </img>
        </div>
      </div>
    );
  }
}
