import React, { Component, PropTypes } from "react";
import { Router, Route, Link, RouteHandler } from "react-router";
import { connect } from "react-redux";
import { createBrowserHistory } from "history";
import Helmet from "react-helmet";
import * as authActions from "redux/modules/auth";
import header from "../../../static/myntraHeader2.png";
import homePage from "../../../static/myntraHomePage.png";

@connect((state) => ({ user: state.auth.user }), authActions)
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
  };

  render() {
    const { user, logout } = this.props;
    const styles = require("./Login.scss");
    return (
      <div className={styles.loginPage}>
        <img src={header} className={styles.myntraHeader}>
          {" "}
        </img>
        <div className={styles.bannerText}>
          <Link to="/cart">OCCASIONS</Link>
        </div>
        <div>
          <img src={homePage} className={styles.myntraHomepage}>
            {" "}
          </img>
        </div>
      </div>
    );
  }
}
